import Head from 'next/head'
import styles from '../styles/Home.module.scss'

export default function Home() {
  return (
    <div
      className={styles.root}
    >
      <Head>
        <title>Alberto racconta Sordi</title>
        <meta
          name="description"
          content="L'autobiografia che non c'era e che ora c'&egrave; proprio per volont&agrave; del grande artista italiano."
        />
        <meta
          name="google"
          content="nositelinkssearchbox"
        />
        <meta
          property="og:url"
          content="https://www.albertoraccontasordi.it"
        />
        <meta
          property="og:type"
          content="website"
        />
        <meta
          name="og:title"
          content="Alberto racconta Sordi"
        />
        <meta
          name="og:site_name"
          content="Alberto racconta Sordi"
        />
        <meta
          property="og:locale"
          content="it"
        />
        <meta
          name="og:description"
          content="L'autobiografia che non c'era e che ora c'&egrave; proprio per volont&agrave; del grande artista italiano."
        />
        <meta
          property="og:image"
          content="https://www.albertoraccontasordi.it/libro.jpg"
        />
        <meta
          property="og:image:alt"
          content="L'autobiografia che non c'era e che ora c'&egrave; proprio per volont&agrave; del grande artista italiano."
        />
        <meta
          name="twitter:card"
          content="summary"
        />
        <meta
          name="twitter:site"
          content="@MariaMschiavina"
        />
        <meta
          name="twitter:url"
          content="https://www.albertoraccontasordi.it"
        />
        <meta
          name="twitter:title"
          content="Alberto racconta Sordi"
        />
        <meta
          name="twitter:description"
          content="L'autobiografia che non c'era e che ora c'&egrave; proprio per volont&agrave; del grande artista italiano."
        />
        <meta
          property="twitter:image"
          content="https://www.albertoraccontasordi.it/libro.jpg"
        />
        <meta
          property="twitter:image:alt"
          content="L'autobiografia che non c'era e che ora c'&egrave; proprio per volont&agrave; del grande artista italiano."
        />
      </Head>

      <main
        className={styles.main}
      >
        <h1
          style={{ display: 'none' }}
        >
          Alberto racconta Sordi
        </h1>
        <section
          className={styles.head}
        >
          <aside
            className={styles.aside}
          >
            <blockquote
              className={styles.quote}
            >
              <p>
                &quot;Un libro di straordinaria <strong>freschezza</strong>.<br />
                Niente pettegolezzi, niente pruriginosi retroscena.&quot;
              </p>
            </blockquote>
            <blockquote
              className={styles.quote}
            >
              <p>
                &quot;In queste pagine trovate le riflessioni <strong>sincere e appassionate</strong> di un uomo che ha interpretato l'anima degli italiani.&quot;
              </p>
            </blockquote>
            <blockquote
              className={styles.quote}
            >
              <p>
                &quot;Un bellissimo racconto in <strong>prima persona</strong>, con tanti divertenti aneddoti.&quot;
              </p>
            </blockquote>
          </aside>
          <figure
            className={styles.headFigure}
          >
            <img
              src="/libro.jpg"
              alt="Alberto racconta Sordi - Il libro"
              title="Alberto racconta Sordi"
              className={styles.book}
            />
            <figcaption
              style={{ display: 'none' }}
            >
              Alberto racconta Sordi
            </figcaption>
          </figure>
        </section>
        <section
          className={styles.links}
        >
          <ul
            className={styles.linksList}
          >
            <li
              className={styles.linkItem}
            >
              <a
                href="https://www.librimondadori.it/libri/alberto-racconta-sordi-maria-antonietta-schiavina-alberto-sordi-fondazione/"
                className={styles.linkAnchor}
              >
                <img
                  src="/mondadori.svg"
                  alt="Mondadori"
                  width="250"
                  className={styles.linkImage}
                />
              </a>
            </li>
            <li
              className={styles.linkItem}
            >
              <a
                href="https://www.amazon.it/Alberto-racconta-Sordi-Confidenze-rimpianti/dp/B0888T33ZX"
                className={styles.linkAnchor}
              >
                <img
                  src="/amazon.svg"
                  alt="Amazon"
                  className={styles.linkImage}
                />
              </a>
            </li>
            <li
              className={styles.linkItem}
            >
              <a
                href="https://www.lafeltrinelli.it/libri/alberto-racconta-sordi-confidenze-inedite/9788804721567"
                className={styles.linkAnchor}
              >
                <img
                  src="/ibs.svg"
                  alt="IBS"
                  className={styles.linkImage}
                />
              </a>
            </li>
            <li
              className={styles.linkItem}
            >
              <a
                href="https://www.amazon.it/Alberto-racconta-Sordi-Confidenze-rimpianti/dp/B0888T33ZX/ref=tmm_aud_swatch_0"
                className={styles.linkAnchor}
              >
                <img
                  src="/audible.svg"
                  alt="Audible"
                  className={styles.linkImage}
                />
              </a>
            </li>
          </ul>
        </section>
        <section
          className={styles.author}
        >
          <figure>
            <img
              src="/autrice.jpg"
              alt="Autrice"
            />
            <figcaption
              style={{ display: 'none' }}
            >
              L'Autrice, Maria Antonietta Schiavina
            </figcaption>
          </figure>
          <article
            className={styles.authorArticle}
          >
            <strong
              className={styles.authorTitle}
            >
              L'autrice
            </strong>
            <p>
              Maria Antonietta Schiavina &egrave; nata a Milano. Collabora con riviste femminili, quotidiani e tv. Per Mondadori ha pubblicato <q>Diversi da chi? Normali vite con handicap (1995)</q>
            </p>
            <p>
              Per contatti:<br />
              <a
                href="mailto:mschiavina.g@gmail.com"
              >
                mschiavina.g@gmail.com
              </a>
            </p>
          </article>
        </section>
        <section
          className={styles.audio}
          style={{ display: 'none' }}
        >
          <figure
            className={styles.snippet}
          >
            <figcaption>
              <strong
                className={styles.snippetTitle}
              >
                Il primo incontro
              </strong>
              <p>
                Alberto e Antonietta si incontrano per la prima volta
              </p>
            </figcaption>
            <audio
              controls
              src="/media/cc0-audio/t-rex-roar.mp3">
              Your browser does not support the
              <code>audio</code> element.
            </audio>
          </figure>
          <figure
            className={styles.snippet}
          >
            <figcaption>
              <strong
                className={styles.snippetTitle}
              >
                Il secondo incontro
              </strong>
              <p>
                Lorem ipsum blah blah blah blalbhalbh fapihf ogah ogha oha sodh.
              </p>
            </figcaption>
            <audio
              controls
              src="/media/cc0-audio/t-rex-roar.mp3">
              Your browser does not support the
              <code>audio</code> element.
            </audio>
          </figure>
          <figure
            className={styles.snippet}
          >
            <figcaption>
              <strong
                className={styles.snippetTitle}
              >
                L'ultimo incontro
              </strong>
              <p>
                Lorem ipsum blah blah blah blalbhalbh fapihf ogah ogha oha sodh.
              </p>
            </figcaption>
            <audio
              controls
              src="/media/cc0-audio/t-rex-roar.mp3">
              Your browser does not support the
              <code>audio</code> element.
            </audio>
          </figure>
        </section>
        <section
          className={styles.bottom}
        >
          <div
            className={styles.bottomWrap}
          >
            <figure
              className={styles.bottomFigure}
            >
              <img
                src="/libro.jpg"
                alt="Alberto racconta Sordi - Il libro"
                title="Alberto racconta Sordi"
                width="100%"
              />
              <figcaption
                style={{ display: 'none' }}
              >
                Alberto racconta Sordi
              </figcaption>
            </figure>
            <article
              className={styles.bottomArticle}
            >
              <p>
                La casa di famiglia. Le sorelle. La fede. Gli amori. La beneficenza. Castiglioncello. L'antiquario mancato (per fortuna). Avaro chi? A un passo dall'altare. La sacra pennichella. Silvana Mangano. La radio e il compagnuccio della parrocchietta. I film realizzati e quelli mancati. La voce di Ollio. Federico Fellini. Katia Ricciarelli e Andreina Pagnani. Dentone e la commedia all'italiana. La tv che zozzeria. L'ossessione di esibirsi. Il successo. Il romanesco nei dialoghi. Apprendista portiere a Milano.
              </p>
              <p>
                C'&egrave; tutto questo e molto altro ancora nelle chiacchierate fra Alberto Sordi e Maria Antonietta Schiavina, che le ha registrate e trascritte. L'impareggiabile Albertone nostro, patrimonio dell'arte mondiale e fenomeno del cinema italiano, scelse una brava e affidabile giornalista milanese per lasciarsi andare a una serie di confidenze mai pubblicate, che lette oggi hanno il sapore di un testamento intimo e artistico.
              </p>
              <p>
                Sordi racconta tutto &quot;senza pudore ma con l'obbligo della discrezione&quot;. Racconta della sua meravigliosa famiglia, che solo il tempo riesce a sbriciolare e da cui non vorr&agrave; mai separarsi. Racconta di una Roma incantata e di un'Italia piena di speranza in cui lui gi&agrave; bambino si fa notare per le incredibili doti canore. E poi gli amori: tanti, romantici ed elegantemente sottratti alla becera fame di scoop dei rotocalchi dell'epoca. I rapporti belli e artisticamente fruttuosi con gli altri grandi della sua epoca, da Fellini alla Mangano, a De Sica. I successi dei suoi film che sono pietre miliari della cultura popolare italiana. E ancora tante riflessioni private sui temi dell'esistenza, la fede, la morte, l'amore per gli italiani, tutti argomenti che lui stesso ha descritto al cinema come nessun altro mai.
              </p>
              <p>
                Alberto racconta Sordi &egrave; l'autobiografia che non c'era e che ora c'&egrave; proprio per volont&agrave; del grande artista italiano.
              </p>
              <p>
                Alberto Sordi (Roma, 1920 - 2003) &egrave; stato uno dei più grandi artisti italiani di sempre, con oltre 200 film in carriera in cui ha primeggiato nelle vesti di attore, regista, sceneggiatore, cantante e doppiatore.
              </p>
            </article>
            <ul
              className={styles.bottomLinks}
            >
              <li
                className={styles.bottomLinkItem}
              >
                <a
                  href="https://www.librimondadori.it/libri/alberto-racconta-sordi-maria-antonietta-schiavina-alberto-sordi-fondazione/"
                >
                  <img
                    src="/mondadori.svg"
                    alt="Mondadori"
                    width="250"
                  />
                </a>
              </li>
              <li
                className={styles.bottomLinkItem}
              >
                <a
                  href="https://www.amazon.it/Alberto-racconta-Sordi-Confidenze-rimpianti/dp/B0888T33ZX"
                >
                  <img
                    src="/amazon.svg"
                    alt="Amazon"
                    width="135"
                  />
                </a>
              </li>
              <li
                className={styles.bottomLinkItem}
              >
                <a
                  href="https://www.lafeltrinelli.it/libri/alberto-racconta-sordi-confidenze-inedite/9788804721567"
                >
                  <img
                    src="/ibs.svg"
                    alt="IBS"
                    width="100"
                  />
                </a>
              </li>
              <li
                className={styles.bottomLinkItem}
              >
                <a
                  href="https://www.amazon.it/Alberto-racconta-Sordi-Confidenze-rimpianti/dp/B0888T33ZX/ref=tmm_aud_swatch_0"
                >
                  <img
                    src="/audible.svg"
                    alt="Audible"
                    width="175"
                  />
                </a>
              </li>
            </ul>
          </div>
        </section>
      </main>
      <footer>
      </footer>
    </div>
  )
}
